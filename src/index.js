import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './button.js'
import registerServiceWorker from './registerServiceWorker';

class Button extends React.Component {
    handleClick = () => {
        this.props.onClickFunction(this.props.incrementValue);
    }

    render() {
        return(
            <button onClick={this.handleClick}>
                +{this.props.incrementValue}
            </button>
        );        
    };    
}

const Result = (props) => {
    return(
        <span>{props.number}</span>
    )
}

class App extends React.Component {
    state = { number: 0 };

    increment = (incrementValue) => {
        this.setState((prevState) => ({            
            number: prevState.number + incrementValue           
        }));
    };

    render() {
        return(
            <div>
                <Button onClickFunction={this.increment} incrementValue={1} />
                <Button onClickFunction={this.increment} incrementValue={2} />
                <Button onClickFunction={this.increment} incrementValue={5} />
                <Result number={this.state.number}/>
            </div>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
